FROM ubuntu:jammy

RUN apt-get update && apt-get install -y curl whois netbase

COPY script.sh /

CMD ["bash", "/script.sh"]