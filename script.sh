#!/bin/bash
# ██╗      ██████╗  ██████╗ ███╗   ███╗ █████╗ ██╗███╗   ██╗     ██████╗██╗  ██╗███████╗ ██████╗██╗  ██╗
# ╚██╗     ██╔══██╗██╔═══██╗████╗ ████║██╔══██╗██║████╗  ██║    ██╔════╝██║  ██║██╔════╝██╔════╝██║ ██╔╝
#  ╚██╗    ██║  ██║██║   ██║██╔████╔██║███████║██║██╔██╗ ██║    ██║     ███████║█████╗  ██║     █████╔╝ 
#  ██╔╝    ██║  ██║██║   ██║██║╚██╔╝██║██╔══██║██║██║╚██╗██║    ██║     ██╔══██║██╔══╝  ██║     ██╔═██╗ 
# ██╔╝     ██████╔╝╚██████╔╝██║ ╚═╝ ██║██║  ██║██║██║ ╚████║    ╚██████╗██║  ██║███████╗╚██████╗██║  ██╗
# ╚═╝      ╚═════╝  ╚═════╝ ╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝     ╚═════╝╚═╝  ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝
version=1.0.4

# Check if the environment variable is set
[ -z "$DOMAIN" ] && echo -e "\x1B[1;31mDOMAIN is not set\x1B[0m" && exit_state=1
[ -z "$WEBHOOK_ID" ] && echo -e "\x1B[1;31mWEBHOOK_ID is not set\x1B[0m" && exit_state=1
[ -z "$WEBHOOK_SECRET" ] && echo -e "\x1B[1;31mWEBHOOK_SECRET is not set\x1B[0m" && exit_state=1
[ -z "$exit_state" ] || exit 1

# Executing and Caching the Whois request
report=$(whois $DOMAIN) 

# Verifying the disponibility and Building the embeds
$(echo "$report" | egrep -q '^No match|^NOT FOUND|^Not fo|AVAILABLE|^No Data Fou|has not been regi|No entri')
if [ $? -eq 0 ]
then
  # Domain name is available
  json='{
   "content":"@everyone",
   "embeds":[
      {
         "type":"rich",
         "title":"🕵️ Whos Is ? ✅ **DISPONIBLE** ✅",
         "description":"🔎  Requête pour le nom de domaine : `'$DOMAIN'`\n\n💸  Acheter sur :\n- [Google Domains]( https://domains.google.com/express?d='$DOMAIN')\n- [Domain.com](https://www.domain.com/registration/?flow=domainDFE&search='$DOMAIN'#/domainDFE/1)\n- [GoDaddy](https://fr.godaddy.com/domainsearch/find?checkAvail=1&isc=FRNPEUR19&domainToCheck='$DOMAIN')\n- [OVHcloud](https://www.ovh.com/fr/order/webcloud/#/webCloud/domain/select?domain='$DOMAIN')",
         "footer": {
           "text": "Version\n'$version'"
         },
         "fields":[
            {
               "name":"📥 Status",
               "value":"AVAILABLE"
            },
            {
               "name":"📤 Pending",
               "value":"AVAILABLE"
            }
         ]
      }
   ]
}'
else
  # Domain name is already registered
  status=$(echo "$report" | grep -sw 'status:' | awk '{print $2}')
  pending=$(echo "$report" | grep -sw 'pending:' | awk '{print $2}')
  
  json='{
   "embeds":[
      {
         "type":"rich",
         "title":"🕵️ Whos Is ?",
         "description":"🔎  Requête pour le nom de domaine : `'$DOMAIN'`\n\n💸  Acheter sur :\n- [Google Domains]( https://domains.google.com/express?d='$DOMAIN')\n- [Domain.com](https://www.domain.com/registration/?flow=domainDFE&search='$DOMAIN'#/domainDFE/1)\n- [GoDaddy](https://fr.godaddy.com/domainsearch/find?checkAvail=1&isc=FRNPEUR19&domainToCheck='$DOMAIN')\n- [OVHcloud](https://www.ovh.com/fr/order/webcloud/#/webCloud/domain/select?domain='$DOMAIN')",
         "footer": {
           "text": "Version\n'$version'"
         },
         "fields":[
            {
               "name":"📥 Status",
               "value":"'$status'"
            },
            {
               "name":"📤 Pending",
               "value":"'$pending'"
            }
         ]
      }
   ]
}'
fi

# Execute the webhook request
curl -s -H "Content-Type: application/json" -d "${json}" "https://discord.com/api/webhooks/$WEBHOOK_ID/$WEBHOOK_SECRET"

exit 0
